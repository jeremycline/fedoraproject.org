title: Fedora Silverblue
leadin: The GNOME desktop experience you know and love in an atomic fashion.
description:
  An atomic variant looks, feels, and behaves just like a regular desktop operating system,
  but your updates are delivered as full images of a working system. This makes every installation
  identical to every other, and it will never change while in use. What's more, Silverblue will
  always keep an older version of the system around for you to boot back into, should you need to,
  allowing you to try new programs, desktops, or even complete version upgrades fearlessly!
  <br /><br />
  The atomic design makes a Silverblue system more stable, less prone to bugs, and easier to
  test and develop. Applications are installed via Flatpak completely independent of the base system,
  and CLI tools can utilize the power of containerization with Toolbox.
  <br /><br/>
  Silverblue comes with the popular GNOME desktop and follows the standard 13 month release-cycle,
  making the experience very similar to that of Fedora Workstation.

image:
  src: https://stg.fedoraproject.org/assets/images/fedora-silverblue-light.png
  alt: Fedora Silverblue Image
  width: 400
  height: 300
links:
  - text: Download Now
    url: /atomic-desktops/silverblue/download/
  - text: Documentation
    url: "https://docs.fedoraproject.org/en-US/fedora-silverblue/"
header_images:
  - image: assets/images/workstation_framework.png
    alt_text: Screenshot of Fedora Silverblue
sections:
  - sectionTitle: Why Fedora Silverblue?
    content:
      - title: Reliable
        description: Each version is updated for approximately 13 months, and each update takes effect on your next reboot, keeping your system consistent. You can even keep working while the updates are being applied!
      - title: Atomic
        description: The whole system is updated in one go, and an update will not apply if anything goes wrong, meaning you will <i>always</i> have a working computer.
      - title: Safe
        description: A previous version of your system is always kept around, just in case. If you need to go back in time, you can!
      - title: Containerized
        description: Graphical applications are installed via Flatpak, and keep themselves separate from the base system. They also allow for fine-grained control over their permissions.
      - title: Developer-Friendly
        description: Toolbx keeps all of your development tools neatly organized per-project. Keep multiple versions of your tools independent from each other and unaffected by changes to the base system.
      - title: Private, Trusted, Open Source
        description: There are no ads, and all your data belongs to you! Fedora is built on the latest open source technologies, and backed by Red Hat.
  - sectionTitle: Built On Top of the Best
    content:
      - title: GNOME Desktop
        description: A beautiful, high-quality desktop, built on the latest open source technology.
        image: public/assets/images/atomic-desktops/gnome-desktop-square.png
      - title: rpm-ostree
        description: rpm-ostree is a hybrid image/package system. It combines libostree and libdnf to provide atomic and safe upgrades with local RPM package layering.
        image: public/assets/images/atomic-desktops/rpm-ostree-square.png
      - title: Applications as Flatpaks
        description: Download thousands of open-source and proprietary, containerized applications thanks to Flatpak.
        image: public/assets/images/atomic-desktops/flatpak-apps-faded-square.png
      - title: Toolbx for Development
        description: Easily create OCI containers which are fully-integrated with your host system, enabling seamless usage of both GUI and CLI tools.
        image: public/assets/images/atomic-desktops/toolbox-square.png
      - title: Fedora Community & Support
        description: Fedora creates an innovative, free and open source platform for hardware, clouds, and containers that enables software developers and community members to build tailored solutions for their users.
        image: public/assets/images/atomic-desktops/fedora-square.png
  - sectionTitle: Related Projects
    content:
      - title:
        description: An atomic desktop operating system featuring the modern and modular Plasma Desktop
        image: public/assets/images/fedora-kinoite-light.png
        url: /atomic-desktops/kinoite
      - title:
        description: An atomic desktop operating system featuring the keyboard-driven Sway window manager.
        image: public/assets/images/fedora-sericea-light.png
        url: /atomic-desktops/sway
      - title:
        description: Fedora CoreOS is an automatically updating, minimal, monolithic, container-focused operating system, designed for clusters but also operable standalone, optimized for Kubernetes but also great without it.
        image: public/assets/images/coreos-logo-light.png
        url: /coreos
