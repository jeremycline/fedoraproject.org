path: flock
title: The Fedora Project Conference
description: ""
header_images:
  - image: public/assets/images/flocklogo-white.png
    alt: Stylized text of the word flock with feathers coming out of the bottom of
      the f and the top of the k
  - image: public/assets/images/2024-rochester-new-york-header.jpg
    alt: "The Rochester, NY skyline as seen above the Broad Street Bridge (not
      pictured). Visible on the right is the a white hotel high rise with the
      Hyatt logo on it and some smaller buildings in the foreground. To the left
      of the image are some similarly tall, darker buildings with no logos, one
      of which is a more traditional looking brock warehouse style building "
    attribution: Rochester photograph by [Wikimedia Commons, the free media
      repository](https://commons.wikimedia.org/wiki/File:Rochester_NY_Broad_Street_Bridge_2002.jpeg).
      Used under the [Creative Commons Attribution-ShareAlike 4.0 International
      license](https://creativecommons.org/licenses/by-sa/4.0/).
links:
  - text: 2024 conference information
    url: /flock/2024
events:
  sectionTitle: Events
  enabled: false
  content:
    - image: public/assets/images/flock_nest_logo.png
      title: Virtual
      description: A 3 day conference hosted on Hopin.io
    - image: public/assets/images/flock_hatch_logo.png
      title: Interested in hosting your own event?
      description: In the month leading up to Flock, Fedorans are encouraged to
        organize local community events. Set the format and topic of your
        choice. The Fedora Council will help support your initiative with
        financial and organizating aid. Click below to set up a Hatch!
explore:
  sectionTitle: Explore the latest in Open Source
  enabled: true
  content:
    - title: Workshops & Hackfests
      description: Workshops and hackfests offer opportunities for active learning and
        collaboration. These sessions are interactive and get you hands-on with
        engineering and non-engineering aspects of the Fedora community. These
        typically run for either two hours (workshops) or a half-day
        (hackfests).
      image: public/assets/images/flock_workshops_logo.png
      link:
        text: ""
        url: ""
    - title: Sessions & Presentations
      image: public/assets/images/flock_sessions_logo.png
      description: Presentations that showcase the work done in Fedora and planning
        for the future. These typically run for either 30 minutes or 60 minutes.
      link:
        text: ""
        url: ""
    - title: Social Events
      image: public/assets/images/flock_socialevents_logo.png
      description: Casual, facilitated, and interactive events for meeting and
        socializing with Fedorans. Spend time with fellow community members to
        get to know each other better and build connections that last throughout
        the year.
      link:
        text: ""
        url: ""
watch:
  sectionTitle: Watch Footage from Past Events
  enabled: true
  content:
    - title: Fedora Project YouTube Channel
      image:
        path: public/assets/images/matthew_talking.png
        alt: A screenshot from the flock 2023 state of fedora talk with the flock logo
          in the top right, Colur in the bottom right and a video frame of
          Matthew Miller speaking from behind a podium above some sponsor logos
      description: There's always something exciting happening within Fedora. This
        includes recordings from previous events and other fun content. Check
        out the Fedora Project's YouTube channel to see what we are up to!
      link:
        text: Visit Fedora Youtube
        url: https://www.youtube.com/fedoraproject
    - link:
        url: https://youtube.com/playlist?list=PL0x39xti0_64OcXEGLCtoI4nouADqaTcT
        text: View the Playlist
      title: Previous Year's Archive
      description: Flock 2023 was held in Cork, Ireland. Check out the recordings from
        the event in the playlist below!
      image:
        path: public/assets/images/flock-2023-thumbnail-frame.png
        alt: "The thumbnail for the Flock 2023 playlist with the Fedora mascot Colur in
          the corner playing a hockey-like game. The Flock logo is in the center
          with a cork, ireland banner as well as the logos of the main sponsirs
          and social media information such as #flocktofedora #flockireland
          fosstodon.org/fedora #flock:fedoraproject.org flock2023.sched.org and
          flocktofedora.org"
  images: ""
showcase:
  sectionTitle: An In-Person Experience
  enabled: true
  sectionDescription: Flock is the Fedora Project's annual contributor-focused
    conference. It typically alternates between European and North American
    locations. The conference provides a venue for face-to-face meetings and
    conversations. It is also a place to celebrate our community. At Flock, we
    communicate the Fedora strategy, make connections that lead to action, and
    celebrate our successes and our community.
  content: []
  images:
    - path: public/assets/images/people-presenting.png
      alt: Two people weaing purple shirts standing behind a podium with a laptop
        presenting at a conference
    - path: public/assets/images/two-people-at-conf.png
      alt: Two people standing in front of a notice board smiling while wearing
        conference badges
    - path: public/assets/images/img_9317.jpg
      alt: A panel of people sitting in a semicircle at the front of a room taking
        questions from an audience
    - path: public/assets/images/people-posing-destination-bg.png
      alt: Two people smiling for the camera outside on a cloudy day. One is wearing a
        black shirt with the red hat shadowman logo and making a "peace" sign
        while the other is wearing a red polo and holding a drink
    - path: public/assets/images/img_9377.jpg
      alt: A group of about 15 people all standing for a photo in front of a projector
        screen with a slide that says 'fedora globalization flock 2016`
community:
  sectionTitle: Connect with us
  enabled: true
  image: public/assets/images/iot_flock_background.jpg
  sectionDescription: |
    Stay connected during Flock through the following channels.
  content:
    - title: Social Media
      image: public/assets/images/twitter_icon.png
      description: Use the **\#FlockToFedora** and **\#FlockRochester** hashtags on
        your preferred social media platforms. Follow Fedora on
        [Mastodon](https://fosstodon.org/@fedora) and
        [Linkedin](https://www.linkedin.com/company/fedora-project/) for
        updates.
    - title: Matrix
      image: public/assets/images/matrix_icon.png
      description: "There are two Matrix rooms for flock:
        **[\\#flock:fedoraproject.org](https://matrix.to/#/#flock:fedoraproject\
        .org)** (high-traffic, attendee chat) and
        **[\\#flock-announce:fedoraproject.org](https://matrix.to/#/#flock:fedo\
        raproject.org)** (low traffic, important announcements). If you do not
        have a Matrix account, you can [get a Matrix account with your Fedora
        Account System login](https://chat.fedoraproject.org/) or sign up to
        [any other public homeserver](https://joinmatrix.org/) of your choice!"
faq:
  sectionTitle: FAQs
  enabled: true
  content:
    - title: Who comes to Flock?
      description: While Flock is open to anyone, the majority of talks at Flock are
        focused on existing contributors and those looking to increase their
        involvement. There is almost no solely user-focused content. Flock is a
        space meant for working on and advancing the interests of our global,
        diverse contributor community.
    - title: What will we do at Flock?
      description: >-
        **Communicate the Fedora Strategy**: Everyone in Fedora should
        understand and feel well-aligned with the project's goals, both tactical
        and long-term. Flock to Fedora is a crucial unifying force for our
        community. It offers a unique venue to discuss new ideas with our
        diverse community around the world. In-person connections form the most
        powerful way to get everyone inspired to work towards our shared goals.
        This makes it the best venue to encourage and motivate our contributors
        toward reaching our shared goals.


        **Make Connections that Lead to Action:** We do amazing work as a global project, but it is hard to know everything that is going on. When we work online, it is easy to focus only on our work and the people we directly work with. At Flock, we meet many other members of the community who we might not otherwise encounter. These unique connections formed in the Flock “melting pot” empower future collaborations and inspire cross-team ideas that couldn’t happen otherwise.


        **Celebrate Our Successes and Our Community:** The Fedora community is made up of diverse people from various professional backgrounds, cultures, languages, time zones, and interests. With this amazing blend of people, we never lose sight of the strong value of our *Friends* Foundation. Flock to Fedora demonstrates how we live that value as a community. The social energy generated from Flock to Fedora sustains contributors for the rest of the year, keeping us connected and engaged.
      endDate: ""
  footerText: Looking for a date that is not listed? Click here for help.
sponsors:
  title: Previous Sponsors
  description: Thank you to all the sponsors who have helped make Flock possible
    over the years.
  content:
    - name: Red Hat
      image: public/assets/images/sponsors/Red_Hat_logo.png
      level: platinium
    - level: gold
      name: CentOS
      image: public/assets/images/sponsors/centos_logo.png
    - level: gold
      name: Rocky Linux
      image: public/assets/images/sponsors/rocky-linux_logo.png
    - level: gold
      name: Lenovo
      image: public/assets/images/sponsors/Lenovo_logo.png
    - name: Microsoft Azure
      image: public/assets/images/sponsors/microsoft-azure_logo.png
      level: gold
    - name: AlmaLinux
      image: public/assets/images/sponsors/AlmaLinux_logo.png
      level: silver
    - name: OpenSuse
      image: public/assets/images/sponsors/opensuse_logo.png
    - name: Meta
      image: public/assets/images/sponsors/meta.png
    - name: DasKeyboard
      image: public/assets/images/sponsors/daskeyboard_logo.png
    - name: Gnome
      image: public/assets/images/sponsors/Gnome_logo.png
    - name: KDE
      image: public/assets/images/sponsors/KDE_logo.png
    - name: OpenSource.com
      image: public/assets/images/sponsors/opensource.com_logo.png
    - name: TuxDigital
      image: public/assets/images/sponsors/TuxDigital_logo.png
    - name: Ansible
      image: public/assets/images/sponsors/ansible_logo.svg
    - name: Adafruit
      image: public/assets/images/sponsors/adafruit_logo.svg
    - name: Apache Software Foundation
      image: public/assets/images/sponsors/apache-software-foundation.png
    - name: ARM
      image: public/assets/images/sponsors/arm_logo_2017.svg
    - name: Amazon Web Services
      image: public/assets/images/sponsors/aws.png
    - name: Bluehost
      image: public/assets/images/sponsors/bluehost_logo_2019.svg
    - name: Citrix
      image: public/assets/images/sponsors/citrix_systems_logo.svg
    - name: College of Charleston
      image: public/assets/images/sponsors/collegeofcharleston_logo-cropped.png
    - name: Datto
      image: public/assets/images/sponsors/datto_logo.png
    - name: Dell
      image: public/assets/images/sponsors/dell.gif
    - name: Destination Linux Network
      image: public/assets/images/sponsors/dln.png
    - name: It’s FOSS
      image: public/assets/images/sponsors/itsfoss.png
    - name: Linux Foundation
      image: public/assets/images/sponsors/lf_logo_new_pantone.png
    - name: Lulzbot
      image: public/assets/images/sponsors/lulzbot-logo.png
    - name: GitLab
      image: public/assets/images/sponsors/Gitlab_logo.png
    - name: Google
      image: public/assets/images/sponsors/google_2015_logo.svg
    - name: OpenNMS
      image: public/assets/images/sponsors/opennms_logo_2021.svg
    - name: O’Reilly
      image: public/assets/images/sponsors/o-reilly_logo_-2019-.svg
    - name: Puppet Labs
      image: public/assets/images/sponsors/puppet.svg
    - name: Rackspace
      image: public/assets/images/sponsors/rackspace_technology.svg
    - name: Spigot
      image: public/assets/images/sponsors/spigot.png
    - name: Network Box
      image: public/assets/images/sponsors/network-box-logo-square_530px.png
  sponsorship:
    link: https://flocktofedora.org/static/images/sponsor-prospectus-2024.pdf
    linktitle: Become a Sponsor
